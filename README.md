# Laravel SQL Reporter

[![Latest Version on Packagist](https://img.shields.io/packagist/v/onlime/laravel-sql-reporter.svg)](https://packagist.org/packages/onlime/laravel-sql-reporter)
[![Packagist Downloads](https://img.shields.io/packagist/dt/onlime/laravel-sql-reporter.svg)](https://packagist.org/packages/onlime/laravel-sql-reporter)
[![PHP from Packagist](https://img.shields.io/packagist/php-v/onlime/laravel-sql-reporter.svg)](https://packagist.org/packages/onlime/laravel-sql-reporter)
[![Build Status](https://github.com/onlime/laravel-sql-reporter/actions/workflows/ci.yml/badge.svg)](https://github.com/onlime/laravel-sql-reporter/actions/workflows/ci.yml)
[![GitHub License](https://img.shields.io/github/license/onlime/laravel-sql-reporter.svg)](https://github.com/onlime/laravel-sql-reporter/blob/main/LICENSE)

This module allows you to log SQL queries to log file in Laravel framework.

**This package is hosted on GitHub: [onlime/laravel-sql-reporter](https://github.com/onlime/laravel-sql-reporter)**
